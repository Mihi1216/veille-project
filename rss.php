<?php
    header('Content-Type: application/rss+xml');
    include "includes/globals/db_connection.php";
    echo "<?xml version=\"1.0\" encodage=\"UTF-8\"?>";
?>

<rss version="2.0">
    <channel>
        <title>Les veilles en FLUX RSS de MIHIANA MAITUI</title>
        <description>Mon blog de "Veille" en Flux RSS</description>
            <?php
                $row = $vl->fetchAll(PDO::FETCH_ASSOC);
                for ($i=0; $i < sizeof($row); $i++) {
                    ?>
                    <item>
                        <title><?php echo ($row[$i]['sujet']); ?></title>
                        <description><?php echo (substr($row[$i]['synthax'], 0, 1000).'...') ?></description>
                        <pubDate><?php echo $row[$i]['date_post'] ?></pubDate>
                        <link><?php echo $row[$i]['liens'] ?></link>
                        <image>
                           <url><?php echo $row[$i]['image'] ?></url>
                        </image>
                    </item>
                    <?php } ?>
    </channel>
</rss>

