<?php
// On détermine sur quelle page on se trouve
if(isset($_GET['page']) && !empty($_GET['page'])){
    $currentPage = (int) strip_tags($_GET['page']);
}else{
    $currentPage = 1;
}
// On se connecte à là base de données
require_once('./includes/globals/db_connection.php');

// On détermine le nombre total d'articles
$sql = 'SELECT COUNT(*) AS veille FROM `veilles`;';

// On prépare la requête
$query = $pdo->prepare($sql);

// On exécute
$query->execute();

// On récupère le nombre d'articles
$result = $query->fetch();

$nbveilles = (int) $result['veille'];

// On détermine le nombre d'articles par page
$parPage = 3;

// On calcule le nombre de pages total
$pages = ceil($nbveilles / $parPage);

// Calcul du 1er article de la page
$premier = ($currentPage * $parPage) - $parPage;

$sql = 'SELECT * FROM `veilles` ORDER BY `id` DESC LIMIT :premier, :parpage;';

// On prépare la requête
$query = $pdo->prepare($sql);

$query->bindValue(':premier', $premier, PDO::PARAM_INT);
$query->bindValue(':parpage', $parPage, PDO::PARAM_INT);

// On exécute
$query->execute();

// On récupère les valeurs dans un tableau associatif
$veilles = $query->fetchAll(PDO::FETCH_ASSOC);

require_once('paginate.php');
?>

<div class="row mt centered">
<?php foreach($veilles as $veille){?>
  <?php for($i = 3; $i < sizeof($veille); $i++) ?>
  <div class="col-lg-4 desc">
    <a class="b-link-fade b-animate-go" href="<?= $veille['link'] ?>" target=_blank><img width="350" height="200" src="<?= $veille['images'] ?>" alt="" />
                <div class="b-wrapper">
                      <h4 class="b-from-left b-animate b-delay03"><?= UTF8_encode($veille['topic']); ?></h4>
                    </div>
                  </a>
                  <p><?= $veille['date_post'] ?></p><i class="fa fa-heart-o"></i>
                </div> 
<?php ?>                        
<?php } ?>
</div>
<!-- /container -->
                <nav>
                    <ul class="pagination">
                        <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
                        <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                            <a href="./index.php?page=<?= $currentPage - 1 ?>" class="page-link"><<</a>
                        </li>
                        <?php for($page = 1; $page <= $pages; $page++): ?>
                          <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                          <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                                <a href="./index.php?page=<?= $page ?>" class="page-link"><?= $page ?></a>
                            </li>
                        <?php endfor ?>
                          <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
                          <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
                            <a href="./index.php?page=<?= $currentPage + 1 ?>" class="page-link">>></a>
                        </li>
                    </ul>
                </nav>
