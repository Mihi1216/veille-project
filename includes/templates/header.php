
<!DOCTYPE html>
<html lang="fr">
<head>
<meta default_charset="utf-8">
<title>Page d'accueil</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Ruda:400,900,700" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="./assets/lib/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="./assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="./assets/lib/animations/animations.css" rel="stylesheet">
<link href="./assets/lib/hover-pack/hover-pack.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="./assets/css/styles.css" rel="stylesheet">
</head>
<body>
<div id="headerwrap">
    <div class="container">
      <div class="row centered">
        <div class="col-lg-8 col-lg-offset-2">
          <h1 class="animation slideDown">Le petit blog de "Veille"</h1>
          <p class="mt"><button type="button" class="btn btn-cta btn-lg"><a href="login.php">Connexion</a></button></p>
        </div>
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /headerwrap -->
<div class="container">
<hr><a href="rss.php">Mon Flux RSS</a><hr>
<div class="row mt centered ">
  <div class="col-lg-4 col-lg-offset-4">
    <h3>Actualités récentes</h3>
    <hr>
  </div>
</div>
<!-- /row -->