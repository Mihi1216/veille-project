<?php

include 'includes/globals/db_connection.php';

// récupérer toutes les veilles

?>


<?php include "includes/templates/header.php"; ?>
  
<div class="row">
  <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
  <div class="col-lg-4 col-md-4 col-xs-12">
    <a class="b-link-fade b-animate-go" href="<?php echo $row['link'];?>" target="_blank"><img width="350" height="200" src="<?php echo $row['images']; ?>" alt="" />
                <div class="b-wrapper">
                      <h4 class="b-from-left b-animate b-delay03"><?php echo UTF8_encode($row['topic']); ?></h4>

                    </div>
                  </a>
                  
    <h5><?php echo $row['date_post']; ?></h5>
    <p><strong>Synthèse :</strong><br>
    <?php echo UTF8_encode($row['synthesis']); ?></p>
    <hr-d>
  </div>
  <!-- col-lg-4 -->
  <?php endwhile; ?>

</div>
<!-- /row -->
</div>
<!-- /container -->

<div class="container">
<div class="row mt centered ">
  <div class="col-lg-4 col-lg-offset-4">
    <h3>Actualités passées</h3>
    <hr>
  </div>
</div>
<?php include "includes/templates/paginate.php"; ?>
<!-- /row -->

<div id="copyrights">
<div class="container">
  <p>
    &copy;<strong>Mihiana Maitui</strong>.
  </p>
</div>
</div>
<!-- / copyrights -->

<?php include "includes/templates/footer.php"; ?>
